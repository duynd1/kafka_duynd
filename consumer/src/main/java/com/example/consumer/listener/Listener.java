package com.example.consumer.listener;

import com.example.consumer.model.Messager;
import com.example.consumer.model.Person;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.time.Period;

@Component
public class Listener {
    int i = 0;
    @KafkaListener(topics = "duynd", clientIdPrefix = "duynd",
            containerFactory = "kafkaListenerStringContainerFactory")
    void listener(String data){
        i++;
        System.out.println("alo "+i);
        Gson gson = new Gson();
        Person person = gson.fromJson(data,Person.class);
        System.out.println(person.toString());
    }
}
