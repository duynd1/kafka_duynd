package com.example.consumer.thread;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task1 {

    @Scheduled(fixedRate = 6000, initialDelay = 0)
    public void insertData(){
        System.out.println("task1");
    }
}
