package com.example.consumer.api;

import com.example.consumer.model.User;
import com.example.consumer.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.*;

@RestController
public class Api {
    @Autowired
    UserRepo repo;
    int i =0;
    @PostMapping("/insert")
    public String insertBigdata(@RequestBody User user){
        i++;
        if (i%100000==0){
            System.out.println("ok 100k");
        }
        return "success";
    }
}
