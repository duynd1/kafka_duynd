package com.example.consumer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "duynd_message")
@ToString
public class Messager {
    @Id
    @JsonProperty("id")
    private String id;
    @JsonProperty("content")
    private String content;
    private String sender;
    private Integer room_detail_id;
}
