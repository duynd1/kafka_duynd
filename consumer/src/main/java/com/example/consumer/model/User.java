package com.example.consumer.model;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "duynd_users")
@ToString
@Builder(toBuilder = true)
@Component
public class User {
    @Id
    private String id;
    private String username;
    private String password;
    private String name;
}
